import 'dart:math';

import 'package:design_prototype/Player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';

import 'CustomElevation.dart';
import 'SizeConfig.dart';

void main() async{
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin{
  TabController tabController;
  double deviceWidth = 0.0;
  AnimationController controller;
  Animation<Offset> offset;

  final List<Player> playerScoreList = <Player>[
    Player("01", "cfr**843", "coins.png", 71799),
    Player("02", "Ale**hio", "coins.png", 39475),
    Player("03", "Wik**ker", "coins.png", 19918),
    Player("04", "hay**870", "coins.png", 10301),
    Player("05", "car**i82", "coins.png", 5353),
    Player("06", "ali**lss", "coins.png", 4876),
    Player("07", "ama**ion", "coins.png", 4322),
    Player("08", "jaz**449", "coins.png", 3987),
    Player("09", "Fra**017", "coins.png", 3029),
    Player("10", "Tes**922", "coins.png", 2342),
  ];

  final List<Tab> myTabs = <Tab>[
    Tab(
      //text: "My Score",
      child: Align(
        alignment: Alignment.topCenter,
        child: Text(
          "My Score",
          style: TextStyle(
              fontSize: 14
          ),
        ),
      ),
    ),
    Tab(
      //text: "Top Players",
      child: Align(
        alignment: Alignment.topCenter,
        child: Text(
          "Top Players",
          style: TextStyle(
              fontSize: 14
          ),
        ),
      ),
    ),
    Tab(
      //text: "Rules",
      child: Align(
        alignment: Alignment.topCenter,
        child: Text(
          "Rules",
          style: TextStyle(
              fontSize: 14
          ),
        ),
      ),
    ),
  ];


  @override
  void initState(){
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    tabController = TabController(
      length: myTabs.length,
      vsync: this,
    );
  }

  @override
  dispose(){
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    final textScaleFactor = MediaQuery.of(context).textScaleFactor;
    final devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    print("Height : $height, Width : $width, textScaleFactor: $textScaleFactor, devicePixelRatio : $devicePixelRatio");
    SizeConfig().init(context);
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
    });
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              topContainer(),
              SizedBox(height: 20),
              taskListContainer(height, width),
              SizedBox(height: 20),
              tabBarContainer(height)
            ],
          ),
        )
    );
  }

  _tabBarView(double height) {
    return AnimatedBuilder(
      animation: tabController.animation,
      builder: (BuildContext context, snapshot) {
        return Transform.translate(
          offset: Offset(0.0, 1.0),
          child: [
            Container(
                height: 640,
                child: myScoreTabContainer(height)
            ),
            Container(
              height: 1185,
              margin: EdgeInsets.only(top: 30),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  topPlayerTabContainer(),
                  SizedBox(height: 40),
                  bottomShareContainer()
                ],
              ),
            ),
            Container(
              height: 600,
              margin: EdgeInsets.only(top: 30),
              child: Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    "Rules body",
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 16
                    ),
                  )
              ),
            ),
          ][tabController.animation.value.round()],
        );
      },
    );
  }

  Widget tabBarContainer(double height){
    //print("tabController.index ${_tabController.index}");
    return DefaultTabController(
      length: 3,
      child: Container(
        //height: tabController.index == 1 ? SizeConfig.blockSizeVertical*168 :SizeConfig.blockSizeVertical*84.3,
        height: tabController.index == 1 ? height*1.665 : height*0.845,
        //constraints:  BoxConstraints.expand(height: _tabController.index == 1 ? 1185 : 600,),
        child: Column(
          children: <Widget>[
            Container(
              //constraints: BoxConstraints.expand(height: 35),
              constraints: BoxConstraints.expand(height: 85),
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: TabBar(
                controller: tabController,
                onTap: (currentIndex){
                  setState(() {
                    tabController.index = currentIndex;
                  });
                },
                tabs: myTabs,
                labelColor: Colors.white,
                unselectedLabelColor: Color(0xFF7F7F7F),
                indicator: BoxDecoration(
                    image: DecorationImage(
                      //image: AssetImage("assets/images/my-score.png"),
                      image: AssetImage("assets/images/tabbar_bg.png"),
                      fit: BoxFit.cover
                    )
                ),
              ),
            ),
            Expanded(
              child: Container(
                transform: Matrix4.translationValues(0.0, -34.0, 0.0),
                child: _tabBarView(height)
                /*TabBarView(
                    controller: tabController,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Container(
                          height: 600,
                          child: myScoreTabContainer(width)
                      ),
                      Container(
                        height: 1185,
                        margin: EdgeInsets.only(top: 30),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            topPlayerTabContainer(),
                            SizedBox(height: 40),
                            bottomShareContainer()
                          ],
                        ),
                      ),
                      Container(
                        height: 600,
                        margin: EdgeInsets.only(top: 30),
                        child: Align(
                          alignment: Alignment.topCenter,
                            child: Text(
                                "Rules body",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16
                                ),
                            )
                        ),
                      ),
                    ]
                ),*/
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget topPlayerTabContainer(){
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: 15.0),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Color(0xFF3B76E0), Color(0xFF983EFC)]
            ),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
          ),
          child: ListView.separated(
              itemCount: playerScoreList.length,
              shrinkWrap: true, // for dynamic height adjusted
              padding: EdgeInsets.symmetric(vertical: 10.0),
              physics: const NeverScrollableScrollPhysics(),
              separatorBuilder: (context, index) => Divider(
                color: Colors.white,
              ),
              itemBuilder: (context, index){
                return (playerScoreList != null && playerScoreList.length > 0) ?
                Container(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex:1,
                        child: Container(
                          height: 35,
                          width: 35,
                          margin: EdgeInsets.only(right: 20),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                          ),
                          child: Center(
                            child: Text(
                                playerScoreList[index].number,
                                style: TextStyle(
                                  color: Color(0xFF3D60E3),
                                  fontWeight: FontWeight.w700
                                ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex:0,
                        child: Text(
                          playerScoreList[index].id,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                      //SizedBox(width: 20),
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image(
                              height: 20,
                              image: AssetImage(
                                "assets/images/${playerScoreList[index].image}"
                              ),
                            ),
                            SizedBox(width: 5),
                            Text(
                              playerScoreList[index].score.toString(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ) : Text("No Data");
              }
          ),
        )
      ],
    );
  }

  Widget myScoreTabContainer(double height){
    return Column(
      children: <Widget>[
        Align(
            alignment: Alignment.topCenter,
            child: Text(
              "My Score 20",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0
              ),
            )
        ),
        SizedBox(height: 11,),
        Container(
          height: SizeConfig.safeBlockVertical*11.8,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/my_score02.png")
              )
          ),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 45),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "05",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(height: 5.0,),
                    Text(
                      "(Share)",
                      style: TextStyle(
                          fontSize: 15.0,
                          color: Color(0xFF8EB8F1),
                          fontWeight: FontWeight.bold
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 45,
                  child: VerticalDivider(
                    color: Colors.white,
                    thickness: 1.0,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "05",
                      style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(height: 5.0,),
                    Text(
                      "(Register)",
                      style: TextStyle(
                          fontSize: 15.0,
                          color: Color(0xFFA4AFF5),
                          fontWeight: FontWeight.bold
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 45,
                  child: VerticalDivider(
                    color: Colors.white,
                    thickness: 1.0,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "05",
                      style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(height: 5.0,),
                    Text(
                      "(Share)",
                      style: TextStyle(
                          fontSize: 15.0,
                          color: Color(0xFFC5A2FC),
                          fontWeight: FontWeight.bold
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        SizedBox(height: 40),
        bottomShareContainer()
      ],
    );
  }

  Widget bottomShareContainer(){
    return Column(
      children: <Widget>[
        Text(
          "Share More and Get More",
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16
          ),
        ),
        SizedBox(height: 30),
        Image(
          image: AssetImage(
              "assets/images/img.png"
          ),
        ),
        SizedBox(height: 20),
        Container(
          height: 45,
          width: 200,
          //margin: EdgeInsets.symmetric(horizontal: 40),
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
                side: BorderSide(color: Color(0xFF983EFC)),
                borderRadius: BorderRadius.circular(30)),
            color: Color(0xFF983EFC),
          ),
          child: MaterialButton(
            onPressed: (){},
            child: Text(
              "Share Now",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget taskListContainer(double height, double width){
    return Center(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: <Widget>[
            taskListHeader(),
            SizedBox(height: 15,),
            listRow("task_img01.png", "Order's Reviews", "+2", "T", "Go", width),
            listRow("task_img02.png", "Add a Credit card", "+10", "T", "Add", width),
            listRow("task_img03.png", "Add Complete Address", "+10", "T", "Add", width),
            listRow("task_img04.png", "Add Profile Picture", "+10", "T", "Add", width),
            listRow("task_img05.png", "Rate The App", "+10", "T", "Add", width),
            listRow("task_img06.png", "Make a Purchase", "+10", "J", "Add", width),
          ],
        ),
      ),
    );
  }

  Widget listRow(String imagePath, String title, String plusCounter, String switchName, String buttonText, double width){
    return CustomElevation(
      height: 112,
      child: Card(
        elevation: 0,
        margin: EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 10.0),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 18.0, 10.0, 18.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 5,
                      blurRadius: 6,
                      offset: Offset(0, 5), // changes position of shadow
                    ),
                  ],
                ),
                child: Image(
                  width: 65,
                  image: AssetImage("assets/images/${imagePath}",),
                ),
              ),
              SizedBox(width: SizeConfig.safeBlockHorizontal*3),
              //SizedBox(width: 10),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    title,
                    style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.w500,
                        color: Color(0xFF535457)
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              color: Color(0xFF76B6FA),
                              borderRadius: BorderRadius.all(Radius.circular(40)),
                            ),
                            height: 20,
                            width: 48,
                          ),
                          Container(
                            height: 20,
                            width: 20,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color(0xFF8EC4FD),
                              border: Border.all(color: Colors.white, width: 1.5),
                            ),
                            child: Center(
                              child: Text(
                                switchName,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 11
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 22,
                            top: 4,
                            child: Text(
                              plusCounter,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 10
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(width: SizeConfig.safeBlockHorizontal * 30),
                      //SizedBox(width: width*0.2916),
                      InkWell(
                        onTap: () => print('hello'),
                        child: new Container(
                          width: 70.0,
                          height: 30.0,
                          decoration: new BoxDecoration(
                            color: Colors.transparent,
                            border: new Border.all(color: Color(0xFF8EC4FD), width: 1.0),
                            borderRadius: new BorderRadius.circular(5.0),
                          ),
                          child: new Center(
                            child: new Text(
                              buttonText.toUpperCase(),
                              style: new TextStyle(
                                  fontSize: 14.0,
                                  color: Color(0xFF8EC4FD)
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget taskListHeader(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          "Task",
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "More",
              style: TextStyle(
                  fontSize: 18,
                  fontStyle: FontStyle.normal,
                  color: Color(0xff545454)
              ),
            ),
            SizedBox(width: 10),
            Align(
                alignment: Alignment.center,
                child: Image(
                  height: 13,
                  width: 13,
                  image: AssetImage(
                      "assets/images/more.png"
                  ),
                )
            ),
          ],
        ),
      ],
    );
  }

  Widget topContainer() {
    return Container(
      height: SizeConfig.safeBlockVertical*63.112,
      //height: 432.0,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/top_bg.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 40),
          Center(
            child: InkWell(
              onTap: (){

              },
              child: Stack(
                children: <Widget>[
                  Image(
                    height: 145,
                    width: 145,
                    image: AssetImage(
                        "assets/images/check_In.png"
                    ),
                  ),
                  Positioned(
                    top: 55,
                    left: 45,
                    child: Center(
                      child: Text(
                        "Check In",
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.w600
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 48),
          Container(
            margin: EdgeInsets.symmetric(horizontal: SizeConfig.safeBlockVertical*1.6),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      middleDayContainer("+3", "Day 1"),
                      middleDayContainer("+3", "Day 2"),
                      middleDayContainer("+18", "Day 3"),
                      middleDayContainer("+3", "Day 4"),
                      middleDayContainer("+2", "Day 5"),
                      middleDayContainer("+3", "Day 6"),
                      lastDayContainerWithVerticalLine("+53", "Day 7"),
                    ],
                  ),
                  dividerBetweenTwoRow(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      middleDayContainer("+3", "Day 8"),
                      middleDayContainer("+3", "Day 9"),
                      middleDayContainer("+2", "Day 10"),
                      middleDayContainer("+2", "Day 11"),
                      middleDayContainer("+3", "Day 12"),
                      middleDayContainer("+2", "Day 13"),
                      lastDayContainerWithVerticalLine("+3", "Day 14"),
                    ],
                  ),
                  dividerBetweenTwoRow(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      middleDayContainer("+3", "Day 15"),
                      middleDayContainer("+3", "Day 16"),
                      middleDayContainer("+2", "Day 17"),
                      middleDayContainer("+2", "Day 18"),
                      middleDayContainer("+3", "Day 19"),
                      middleDayContainer("+3", "Day 20"),
                      lastDayContainer("+2", "Day 21"),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
  Widget lastDayContainer(String plusCounter, String day){
    return Container(
      height: 55,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: SizeConfig.blockSizeHorizontal*8,
            width: SizeConfig.blockSizeHorizontal*8,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.transparent,
              border: Border.all(color: Colors.white, width: 2.0),
            ),
            child: Center(
              child: Text(
                plusCounter,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 12
                ),
              ),
            ),
          ),
          SizedBox(height: 5),
          Text(
            day,
            style: TextStyle(
                color: Colors.white,
                fontSize: 10
            ),
          )
        ],
      ),
    );
  }

  Widget lastDayContainerWithVerticalLine(String plusCounter, String day){
    return Container(
      height: 55,
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: SizeConfig.blockSizeHorizontal*8,
                width: SizeConfig.blockSizeHorizontal*8,
                margin: EdgeInsets.only(left: 0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.transparent,
                  border: Border.all(color: Colors.white, width: 2.0),
                ),
                child: Center(
                  child: Text(
                    plusCounter,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5),
              Text(
                day,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 10
                ),
              )
            ],
          ),
          Positioned(
            top: 32,
            left: 9.5,
            child: SizedBox(
              height: 30,
              child: VerticalDivider(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget dividerBetweenTwoRow(){
    return Center(
      child: Padding(
        padding: EdgeInsets.only(left: 15, right: SizeConfig.blockSizeHorizontal*3.8),
        child: Stack(
          children: <Widget>[
            SizedBox(
              height: 17,
              child: VerticalDivider(
                color: Colors.white,
                width: 0,
              ),
            ),
            Divider(
              color: Colors.white,
              height: 0,
            ),

          ],
        ),
      ),
    );
  }

  Widget middleDayContainer(String plusCounter, String day, ){
    return Container(
      height: 55,
      //width: SizeConfig.safeBlockHorizontal*14.217,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: SizeConfig.blockSizeHorizontal*8,//31,
                width: SizeConfig.blockSizeHorizontal*8,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.transparent,
                  border: Border.all(color: Colors.white, width: 2.0),
                ),
                child: Center(
                  child: Text(
                    plusCounter,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: SizeConfig.blockSizeHorizontal*6.2, //18
                child: Divider(
                  color: Colors.white,
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.only(left: 2.0),
            child: Text(
              day,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 10
              ),
            ),
          )
        ],
      ),
    );
  }
}